package com.hundsun.epay.autosimu.core;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.hundsun.epay.autosimu.api.AutoSimuGenerator;
import com.hundsun.epay.autosimu.config.Configuration;
import com.hundsun.epay.autosimu.config.xml.ConfiguratioinParser;

public class TestConfigurationParse {
//	@Test
	public void testParseAutosimuConfiguration() throws Exception{
		List<String> warning = new ArrayList<String>();
		ConfiguratioinParser cp = new ConfiguratioinParser(warning);
		Configuration config = cp.parseConfiguration(this.getClass().getClassLoader().getResourceAsStream("simuConfig.xml"));
		System.out.println("Parse success: " + config);
	}
	
//	@Test
	public void testGenerator() throws Exception{
		List<String> warning = new ArrayList<String>();
		ConfiguratioinParser cp = new ConfiguratioinParser(warning);
		Configuration config = cp.parseConfiguration(this.getClass().getClassLoader().getResourceAsStream("simuConfig.xml"));
		System.out.println("Parse success: " + config);
		System.out.println();
		AutoSimuGenerator generator = new AutoSimuGenerator(config);
		generator.generate();
	}
	
//	@Test
	public void testGeneratorGateway() throws Exception{
		List<String> warning = new ArrayList<String>();
		ConfiguratioinParser cp = new ConfiguratioinParser(warning);
		Configuration config = cp.parseConfiguration(this.getClass().getClassLoader().getResourceAsStream("unionpay_gateway_simuConfig.xml"));
		System.out.println("Parse success: " + config);
		System.out.println();
		AutoSimuGenerator generator = new AutoSimuGenerator(config);
		generator.generate();
	}
	
	@Test
	public void testGeneratorSingleTrade() throws Exception{
		List<String> warning = new ArrayList<String>();
		ConfiguratioinParser cp = new ConfiguratioinParser(warning);
		Configuration config = cp.parseConfiguration(this.getClass().getClassLoader().getResourceAsStream("llpay_collect_simuConfig.xml"));
		System.out.println("Parse success: " + config);
		System.out.println();
		AutoSimuGenerator generator = new AutoSimuGenerator(config);
		generator.generate();
	}
}
