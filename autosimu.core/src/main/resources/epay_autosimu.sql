/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : epay

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2018-04-11 16:25:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for simu_orders
-- ----------------------------
DROP TABLE IF EXISTS `simu_orders`;
CREATE TABLE `simu_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `order_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '订单号',
  `detail_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '明细号',
  `channel_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '通道编号，同配置文件中的id',
  `fields` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '控制域存储',
  `extfld1` varchar(255) DEFAULT NULL COMMENT '扩展字段',
  `extfld2` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '扩展字段',
  `extfld3` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '扩展字段',
  PRIMARY KEY (`id`),
  KEY `order_no_select` (`channel_id`,`order_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='挡板订单表';

