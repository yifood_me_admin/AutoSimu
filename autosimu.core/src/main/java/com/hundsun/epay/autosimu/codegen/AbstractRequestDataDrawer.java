package com.hundsun.epay.autosimu.codegen;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hundsun.epay.autosimu.api.IntrospectedChannel;
import com.hundsun.epay.autosimu.api.IntrospectedFieldControl;
import com.hundsun.epay.autosimu.api.IntrospectedTrade;
import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 请求数据提取器
 * @author Clown
 *
 */
public abstract class AbstractRequestDataDrawer {
	/**
	 * 添加提取请求数据的表达式集合
	 * @param importTypes 导入的类型集合
	 * @param tradeGenerator 交易生成器
	 * @param bodies 方法实现
	 * @return 提取结果，key为内部变量名称
	 */
	public abstract Map<String, String> addDrawRequestDataExpression(Set<FullyQualifiedJavaType> importTypes, AbstractTradeGenerator tradeGenerator, List<String> bodies);

	/**
	 * 返回指定交易应用到的所有的控制域
	 * @param tradeGenerator
	 * @return
	 */
	protected Set<IntrospectedFieldControl> getParamKeyRules(AbstractTradeGenerator tradeGenerator){
		Set<IntrospectedFieldControl> rules = new HashSet<IntrospectedFieldControl>();
		IntrospectedChannel introsChannel = tradeGenerator.getIntrospectedChannel();
		IntrospectedTrade introsTrade = tradeGenerator.getIntrospectedTrade();
		//订单号控制域对象---有且仅有一个
		rules.addAll(introsChannel.getActualIntrospectedFieldControl(introsTrade.getOrderNoControlField()));
		//明细号控制域对象---有一个或没有
		if(StringUtility.stringHasValue(introsTrade.getDetailNoControlField())) {
			rules.addAll(introsChannel.getActualIntrospectedFieldControl(introsTrade.getDetailNoControlField()));
		}
		//应用到当前交易的控制域对象---多个或没有
		if(introsTrade.getCurrentControlField() != null) {
			for (String currentString : introsTrade.getCurrentControlField()) {
				rules.addAll(introsChannel.getActualIntrospectedFieldControl(currentString));
			}
		}
		//不对当前交易有作用的控制域对象，主要用于给关联交易预留---多个或没有
		if(introsTrade.getExportControlField() != null) {
			for (String exportString : introsTrade.getExportControlField()) {
				rules.addAll(introsChannel.getActualIntrospectedFieldControl(exportString));
			}
		}
		//后台通知回调控制域对象---一个或没有
		if(introsTrade.getBackUrlControlField() != null) {
			rules.addAll(introsChannel.getActualIntrospectedFieldControl(introsTrade.getBackUrlControlField()));
		}
		//前台回跳地址控制域对象---一个或没有
		if(introsTrade.getFrontUrlControlField() != null) {
			rules.addAll(introsChannel.getActualIntrospectedFieldControl(introsTrade.getFrontUrlControlField()));
		}
		
		return rules;
	}
}
