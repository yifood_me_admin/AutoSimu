package com.hundsun.epay.autosimu.api.dom.java;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.hundsun.epay.autosimu.api.dom.OutputUtility;

/**
 * 内部接口
 * @author clown
 * @Date 2018-02-05
 */
public class InnerInterface extends JavaElement {
	private List<Field> fields;   //属性
	private FullyQualifiedJavaType type;   //类型
	private Set<FullyQualifiedJavaType> superInterfaceTypes;   //继承接口
	private List<InnerInterface> innerInterfaces;    //内部接口
	private List<Method> methods;    //方法
	
	public InnerInterface(FullyQualifiedJavaType type) {
		super();
		this.type = type;
		this.fields = new ArrayList<Field>();
		this.superInterfaceTypes = new HashSet<FullyQualifiedJavaType>();
		this.innerInterfaces = new ArrayList<InnerInterface>();
		this.methods = new ArrayList<Method>();
	}

	public InnerInterface(String type) {
		this(new FullyQualifiedJavaType(type));
	}

	public FullyQualifiedJavaType getType() {
		return type;
	}

	public void setType(FullyQualifiedJavaType type) {
		this.type = type;
	}

	public List<Field> getFields() {
		return fields;
	}

	public Set<FullyQualifiedJavaType> getSuperInterfaceTypes() {
		return superInterfaceTypes;
	}

	public List<InnerInterface> getInnerInterfaces() {
		return innerInterfaces;
	}

	public List<Method> getMethods() {
		return methods;
	}
	
	public void addMethod(Method method){
		this.methods.add(method);
	}
	
	public void addInnerInterface(InnerInterface innerInterface){
		this.innerInterfaces.add(innerInterface);
	}
	
	public void addSuperInterfaceType(FullyQualifiedJavaType type){
		this.superInterfaceTypes.add(type);
	}
	
	public void addField(Field field){
		this.fields.add(field);
	}
	
	public String getFormattedContent(int indentLevel, CompilationUnit unit){
		StringBuilder sb = new StringBuilder();
		
		//注释注解
		addFormattedJavaDocLines(sb, indentLevel);
		addFormattedAnnotation(sb, indentLevel);
		
		//修饰符
		OutputUtility.javaIndent(sb, indentLevel);
		sb.append(getVisibility().getDesc());
		
		if(isStatic()){
			sb.append("static ");
		}
		
		if(isFinal()){
			sb.append("final ");
		}
		
		//声明
		sb.append("interface ");
		sb.append(type.getShortName());
		
		//接口继承
		if(!superInterfaceTypes.isEmpty()){
			boolean first = true;
			for (FullyQualifiedJavaType fullyQualifiedJavaType : superInterfaceTypes) {
				if(first){
					first = false;
				}else{
					sb.append(", ");
				}
				sb.append(JavaDomUtils.calculateTypeName(unit, fullyQualifiedJavaType));
			}
		}
		
		sb.append(" {");
		indentLevel ++;
		
		//属性
		Iterator<Field> fieldInterator = fields.iterator();
		while(fieldInterator.hasNext()){
			OutputUtility.newLine(sb);
			OutputUtility.javaIndent(sb, indentLevel);
			sb.append(fieldInterator.next().getFormattedContent(indentLevel, unit));
			if(fieldInterator.hasNext()){
				OutputUtility.newLine(sb);
			}
		}
		
		if(!methods.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//方法
		Iterator<Method> methodIterator = methods.iterator();
		while(methodIterator.hasNext()){
			OutputUtility.newLine(sb);
			OutputUtility.javaIndent(sb, indentLevel);
			sb.append(methodIterator.next().getFormattedContent(unit, indentLevel, true));
			if(methodIterator.hasNext()){
				OutputUtility.newLine(sb);
			}
		}
		
		if(!innerInterfaces.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//内部接口
		Iterator<InnerInterface> innerInterfaceIterator = innerInterfaces.iterator();
		while(innerInterfaceIterator.hasNext()){
			OutputUtility.newLine(sb);
			OutputUtility.javaIndent(sb, indentLevel);
			sb.append(innerInterfaceIterator.next().getFormattedContent(indentLevel, unit));
			if(innerInterfaceIterator.hasNext()){
				OutputUtility.newLine(sb);
			}
		}
		
		indentLevel --;
		OutputUtility.newLine(sb);
		OutputUtility.javaIndent(sb, indentLevel);
		sb.append("}");
		
		return sb.toString();
	}
	
	public FullyQualifiedJavaType getSuperClass() {
		// TODO Auto-generated method stub
		return null;
	}
}
