package com.hundsun.epay.autosimu.internal.rules;

import com.hundsun.epay.autosimu.enums.ReservedWord;

public class FieldControlMap {
	protected String resp;
	protected boolean isOvertime = false;
	protected boolean isException = false;
	
	public FieldControlMap(String resp){
		parseResp(resp);
	}
	
	protected void parseResp(String resp){
		//判断是否是保留字
		ReservedWord word = ReservedWord.getWrodByCode(resp);
		if(word == null){
			if(resp.startsWith("^")){
				if(ReservedWord.getWrodByCode(resp.substring(1)) == null){
					this.resp = resp;
				}else{
					this.resp = resp.substring(1);
				}
			}
		}else{//关键字
			if(ReservedWord.OVERTIME == word){//超时
				this.resp = resp;
				this.isOvertime = true;
			}else if(ReservedWord.EXCETPION == word){
				this.resp = "惠付模拟异常";
				this.isException = true;
			}else{//其他关键字同样转义
				throw new RuntimeException("The reserved word need to transfering meaning by '^'");
			}
		}
		
	}

	/**
	 * @return the resp
	 */
	public String getResp() {
		return resp;
	}

	/**
	 * @return the isOvertime
	 */
	public boolean isOvertime() {
		return isOvertime;
	}

	/**
	 * @return the isException
	 */
	public boolean isException() {
		return isException;
	}
}
