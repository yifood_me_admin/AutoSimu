package com.hundsun.epay.autosimu.api.dom.java;

import java.util.ArrayList;
import java.util.List;

/**
 * 参数定义
 * @author clown
 *
 */
public class Parameter {
	private String name;    //参数名
	private FullyQualifiedJavaType type;   //参数类型
	private boolean isFinal;   //final
	private boolean isVarargs;    //变长参数
	/**注解*/
	private List<String> annotations;   //注解
	
	public Parameter(String name, FullyQualifiedJavaType type, boolean isFinal) {
		this.name = name;
		this.type = type;
		this.isFinal = isFinal;
		this.annotations = new ArrayList<String>();
	}

	public Parameter(String name, FullyQualifiedJavaType type) {
		this(name, type, false);
	}

	public Parameter(String name, FullyQualifiedJavaType type,
			String annotation) {
		this(name, type);
		addAnnotation(annotation);
	}

	public Parameter(String name, FullyQualifiedJavaType type, boolean isFinal,
			String annotation) {
		this(name, type, isFinal);
		addAnnotation(annotation);
	}

	public String getName() {
		return name;
	}

	public FullyQualifiedJavaType getType() {
		return type;
	}

	public boolean isFinal() {
		return isFinal;
	}

	public List<String> getAnnotations() {
		return annotations;
	}
	
	public void addAnnotation(String annotation){
		this.annotations.add(annotation);
	}
	
	/**
	 * 格式化输出
	 * @param unit
	 * @return
	 */
	public String getFormattedContent(CompilationUnit unit){
		StringBuilder sb = new StringBuilder();
		for (String annotation : annotations) {
			sb.append(annotation);
			sb.append(" ");
		}
		
		if(isFinal){
			sb.append("final ");
		}
		
		sb.append(JavaDomUtils.calculateTypeName(unit, type));
		sb.append(" ");
		
		if(isVarargs){
			sb.append("... ");
		}
		
		sb.append(name);
		
		return sb.toString();
	}

	@Override
	public String toString() {
		return getFormattedContent(null);
	}
	
	
}
