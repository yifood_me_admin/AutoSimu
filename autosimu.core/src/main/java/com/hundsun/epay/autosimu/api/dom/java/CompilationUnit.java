package com.hundsun.epay.autosimu.api.dom.java;

import java.util.List;
import java.util.Set;

public interface CompilationUnit {
	/**获取编译单元的类型*/
	FullyQualifiedJavaType getType();
	/**import的类型*/
	Set<FullyQualifiedJavaType> getImportedType();
	/**添加import Type*/
	void addImportType(FullyQualifiedJavaType type);
	/**添加import Type*/
	void addImportTypes(Set<FullyQualifiedJavaType> types);
	/**静态导入*/
	Set<String> getStaticImports();
	/**添加静态导入*/
	void addStaticImport(String importEle);
	/**添加静态导入*/
	void addStaticImports(Set<String> importEles);
	/**是否是接口*/
	boolean isJavaInteface();
	/**是否是枚举*/
	boolean isJavaEnumeration();
	/**添加文件注释*/
	void addFileCommentLines(String line);
	/**文件注释*/
	List<String> getFileCommentLines();
	/**格式化输出*/
	String getFormattedContent();
	/**实现接口*/
	Set<FullyQualifiedJavaType> getSuperInterfaceTypes();
	/**父类*/
	FullyQualifiedJavaType getSuperClass();
}
