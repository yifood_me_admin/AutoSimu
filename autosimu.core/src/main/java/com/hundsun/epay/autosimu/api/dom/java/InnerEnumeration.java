package com.hundsun.epay.autosimu.api.dom.java;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.hundsun.epay.autosimu.api.dom.OutputUtility;

/**
 * 内部枚举
 * @author clown
 * @Date 2018-02-04
 */
public class InnerEnumeration extends JavaElement {
	private FullyQualifiedJavaType type;   //类型
	private List<InnerClass> innerClasses;   //内部类
	private List<InnerEnumeration> innerEnums;   //内部枚举
	private Set<FullyQualifiedJavaType> superInterfaceTypes;   //实现的接口
	private List<Method> methods;   //方法
	private List<Field> fields;   //属性
	private List<String> enumConstant;   //枚举常量
	
	public InnerEnumeration(FullyQualifiedJavaType type) {
		super();
		this.type = type;
		this.innerClasses = new ArrayList<InnerClass>();
		this.innerEnums = new ArrayList<InnerEnumeration>();
		this.superInterfaceTypes = new HashSet<FullyQualifiedJavaType>();
		this.methods = new ArrayList<Method>();
		this.fields = new ArrayList<Field>();
		this.enumConstant = new ArrayList<String>();
	}

	public InnerEnumeration(String type){
		this(new FullyQualifiedJavaType(type));
	}

	public FullyQualifiedJavaType getType() {
		return type;
	}

	public void setType(FullyQualifiedJavaType type) {
		this.type = type;
	}

	public List<InnerClass> getInnerClasses() {
		return innerClasses;
	}

	public List<InnerEnumeration> getInnerEnums() {
		return innerEnums;
	}

	public Set<FullyQualifiedJavaType> getSuperInterfaceTypes() {
		return superInterfaceTypes;
	}

	public List<Method> getMethods() {
		return methods;
	}

	public List<Field> getFields() {
		return fields;
	}

	public List<String> getEnumConstast() {
		return enumConstant;
	}
	
	public void addField(Field field){
		this.fields.add(field);
	}
	
	public void addMethod(Method method){
		this.methods.add(method);
	}
	
	public void addInnerClass(InnerClass innerClass){
		this.innerClasses.add(innerClass);
	}
	
	public void addInnerEnumeration(InnerEnumeration innerEnum){
		this.innerEnums.add(innerEnum);
	}
	
	public void addSuperInterfaceType(FullyQualifiedJavaType interfaceType){
		this.superInterfaceTypes.add(interfaceType);
	}
	
	public void addEnumConstant(String enumConstant){
		this.enumConstant.add(enumConstant);
	}

	public String getFormattedContent(int indentLevel, CompilationUnit unit){
		StringBuilder sb = new StringBuilder();
		
		addFormattedJavaDocLines(sb, indentLevel);
		addFormattedAnnotation(sb, indentLevel);
		
		OutputUtility.javaIndent(sb, indentLevel);
		sb.append(getVisibility().getDesc());
		
		sb.append("enum ");
		sb.append(type.getShortName());
		
		if(!superInterfaceTypes.isEmpty()){
			sb.append(" implements ");
			boolean first = true;
			for (FullyQualifiedJavaType interfaceType : superInterfaceTypes) {
				if(first){
					first = false;
				}else{
					sb.append(", ");
				}
				sb.append(JavaDomUtils.calculateTypeName(unit, interfaceType));
			}
		}
		
		sb.append(" {");
		indentLevel ++;
		
		//枚举常量
		for (String enumConstant : enumConstant) {
			OutputUtility.newLine(sb);
			OutputUtility.javaIndent(sb, indentLevel);
			sb.append(enumConstant);
			sb.append(",");
		}
		OutputUtility.newLine(sb);
		OutputUtility.javaIndent(sb, indentLevel);
		sb.append(";");
		
		if(!fields.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//属性
		Iterator<Field> fieldIterator = fields.iterator();
		while(fieldIterator.hasNext()){
			OutputUtility.newLine(sb);
			sb.append(fieldIterator.next().getFormattedContent(indentLevel, unit));
			if(fieldIterator.hasNext()){
				OutputUtility.newLine(sb);
			}
		}
		
		if(!methods.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//方法
		Iterator<Method> methodIterator = methods.iterator();
		while(methodIterator.hasNext()){
			OutputUtility.newLine(sb);
			sb.append(methodIterator.next().getFormattedContent(unit, indentLevel, false));
			if(methodIterator.hasNext()){
				OutputUtility.newLine(sb);
			}
		}
		
		if(!innerClasses.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//内部类
		Iterator<InnerClass> innerClassIterator = innerClasses.iterator();
		while(innerClassIterator.hasNext()){
			OutputUtility.newLine(sb);
			sb.append(innerClassIterator.next().getFormattedContent(indentLevel, unit));
			if(innerClassIterator.hasNext()){
				OutputUtility.newLine(sb);
			}
		}
		
		if(!innerEnums.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//内部枚举
		Iterator<InnerEnumeration> innerEnumIterator = innerEnums.iterator();
		while(innerEnumIterator.hasNext()){
			OutputUtility.newLine(sb);
			sb.append(innerEnumIterator.next().getFormattedContent(indentLevel, unit));
			if(innerEnumIterator.hasNext()){
				OutputUtility.newLine(sb);
			}
		}
		
		indentLevel --;
		OutputUtility.newLine(sb);
		OutputUtility.javaIndent(sb, indentLevel);
		sb.append("}");
		
		return sb.toString();
	}
}
