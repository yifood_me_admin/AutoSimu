package com.hundsun.epay.autosimu.exception;

import java.util.ArrayList;
import java.util.List;

public class XmlParseException extends Exception{

	private static final long serialVersionUID = -2018232123118751715L;

	private List<String> errors;
	
	public XmlParseException(String message) {
		super();
		this.errors = new ArrayList<String>();
		this.errors.add(message);
	}

	public XmlParseException(List<String> errors) {
		super();
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}

	@Override
	public String getMessage() {
		if(this.errors != null && this.errors.size() > 0){
			return this.errors.get(0);
		}
		return super.getMessage();
	}
	
	
}
