package com.hundsun.epay.autosimu.api.dom;

import java.util.Set;
import java.util.TreeSet;

import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;

public class OutputUtility {
	private static final String LINE_SEPARATOR;
	static{
		String ls = System.getProperty("line.separator");
		if(ls == null){
			ls = "\n";
		}
		LINE_SEPARATOR = ls;
	}
	/**
	 * 格式化缩进
	 * @param sb
	 * @param indentLevel
	 */
	public static void javaIndent(StringBuilder sb, int indentLevel){
		for (int i = 0; i < indentLevel; i ++) {
			sb.append("    ");
		}
	}
	
	/**
	 * 换行
	 * @param sb
	 */
	public static void newLine(StringBuilder sb){
		sb.append(LINE_SEPARATOR);
	}
	
	public static Set<String> calculateImportTypes(Set<FullyQualifiedJavaType> types){
		Set<String> improtStrings = new TreeSet<String>();
		StringBuilder sb = new StringBuilder();
		for (FullyQualifiedJavaType fullyQualifiedJavaType : types) {
			for (String importString : fullyQualifiedJavaType.getImportList()) {
				sb.append("import ");
				sb.append(importString);
				sb.append(";");
				improtStrings.add(sb.toString());
				sb.setLength(0);
			}
		}
		return improtStrings;
	}
}
