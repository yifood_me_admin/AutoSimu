package com.hundsun.epay.autosimu.codegen;

import java.util.Set;

import com.hundsun.epay.autosimu.api.IntrospectedFieldControl;
import com.hundsun.epay.autosimu.internal.Genealogy;
import com.hundsun.epay.autosimu.internal.rules.Rules;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * JSON 数据请求提取器
 * @author clown
 *
 * @param <T>
 */
public abstract class AbstractJSONRequestDataDrawer extends
		AbstractRequestDataDrawer {

	protected Genealogy createGenealogyForReqeustKey(Set<IntrospectedFieldControl> fieldControls){
		Genealogy rootGenealogy = Genealogy.getRootGenealogy();
		for (IntrospectedFieldControl introspectedFieldControl : fieldControls) {
			addRuleIntoGenealogy(introspectedFieldControl.getFieldControlKeyRule(), rootGenealogy);
		}
		return rootGenealogy;
	}
	
	private Genealogy addRuleIntoGenealogy(Rules rule, Genealogy parent){
		Genealogy genealogy = null;
		if(rule.getParent() != null){
			genealogy = addRuleIntoGenealogy(rule.getParent(), parent);
			genealogy = genealogy.addChild(rule.getName());
		}else{
			genealogy = parent.addChild(rule.getName());
		}
		
		return genealogy;
	}
}
