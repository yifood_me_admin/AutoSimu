package com.hundsun.epay.autosimu.codegen;

import java.util.List;

import com.hundsun.epay.autosimu.api.IntrospectedChannel;
import com.hundsun.epay.autosimu.config.PayCorporation;

/**
 * 生成器
 * @author clown
 *
 */
public class AbstractGenerator {
	protected List<String> warnings;
	protected PayCorporation payCorp;
	protected IntrospectedChannel introspectedChannel;
	
	public AbstractGenerator() {
		super();
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}

	public PayCorporation getPayCorp() {
		return payCorp;
	}

	public void setPayCorp(PayCorporation payCorp) {
		this.payCorp = payCorp;
	}

	public IntrospectedChannel getIntrospectedChannel() {
		return introspectedChannel;
	}

	public void setIntrospectedChannel(IntrospectedChannel introspectedChannel) {
		this.introspectedChannel = introspectedChannel;
	}
	
}
