package com.hundsun.epay.autosimu.internal.parse;

/**
 * 等于运算符
 * @author Clown
 *
 */
public class EqualOperatorParser extends OperatorParser {
	
	@Override
	public String format() {
		return new StringBuilder().append(getSecondVar()).append(".equals(").append(getFirstVar()).append(")").toString();
	}

	@Override
	public void parseExpression(String expression) {
		int equalChar = expression.indexOf("=");
		String leftValue = expression.substring(0, equalChar);
		String rightValue = expression.substring(equalChar + 1);
		this.setFirstVar(leftValue);
		this.setSecondVar("\"" + rightValue + "\"");
	}

}
