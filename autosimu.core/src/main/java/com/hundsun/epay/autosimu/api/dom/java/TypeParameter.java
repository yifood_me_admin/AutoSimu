package com.hundsun.epay.autosimu.api.dom.java;

import java.util.ArrayList;
import java.util.List;

/**
 * 类型参数，适用于method中,如：
 * public &lt;T extends SuperClass&gt; SuperClass methodName();
 * @author clown
 *
 */
public class TypeParameter {
	private String name;   //类型参数名
	private List<FullyQualifiedJavaType> extendsTypes;   //参数上限
	
	public TypeParameter(String name, List<FullyQualifiedJavaType> extendsTypes) {
		this.name = name;
		this.extendsTypes = extendsTypes;
	}

	public TypeParameter(String name) {
		this(name, new ArrayList<FullyQualifiedJavaType>());
	}

	public String getName() {
		return name;
	}

	public List<FullyQualifiedJavaType> getExtendsTypes() {
		return extendsTypes;
	}
	
	public String getFormattedContent(CompilationUnit unit){
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		if(!extendsTypes.isEmpty()){
			sb.append(" extends ");
			boolean first = true;
			for (FullyQualifiedJavaType type : extendsTypes) {
				if(first){
					first = false;
				}else{
					sb.append(" & ");
				}
				sb.append(JavaDomUtils.calculateTypeName(unit, type));
			}
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return getFormattedContent(null);
	}
}
