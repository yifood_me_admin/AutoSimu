package com.hundsun.epay.autosimu.codegen.jfinal.trade;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import com.hundsun.epay.autosimu.api.IntrospectedFieldControl;
import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.api.dom.java.JavaVisibility;
import com.hundsun.epay.autosimu.api.dom.java.Method;
import com.hundsun.epay.autosimu.api.dom.java.TopLevelClass;
import com.hundsun.epay.autosimu.internal.ObligateInnerVariableName;
import com.hundsun.epay.autosimu.internal.rules.FieldControlRules;
import com.hundsun.epay.autosimu.internal.rules.Rules;

/**
 * 网关交易生成器
 * @author clown
 *
 */
public class GatewayTradeGenerator extends JFinalAbstractTradeGenerator {

	@Override
	public void addTradeImplementsElements(TopLevelClass topClass) {
		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
		Method method = getMethodShell();
		//读取订单号，透传字段参数
		Map<String, String> params = getAbstractRequestDataDrawer().addDrawRequestDataExpression(importedTypes, this, method.getBodyLines());
		
		importedTypes.add(new FullyQualifiedJavaType("com.hundsun.epay.simu.model.Orders"));
//		importedTypes.add(new FullyQualifiedJavaType("com.hundsun.epay.simu.service.OrderService"));
		importedTypes.add(new FullyQualifiedJavaType("java.util.Map"));
		importedTypes.add(new FullyQualifiedJavaType("java.util.HashMap"));
		importedTypes.add(new FullyQualifiedJavaType("com.alibaba.fastjson.JSONObject"));
		
		method.addBodyLine("");
		
		//存库表
		String line = "Orders " + ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + " = new Orders();";
		method.addBodyLine(line);
		line = "Map<String, String> " + ObligateInnerVariableName.OBLIGATE_ORDER_FIELDS + " = new HashMap<String, String>();";
		method.addBodyLine(line);
		for (Entry<String, String> entry : params.entrySet()) {
			if(ObligateInnerVariableName.OBLIGATE_ORDER_NO.equals(entry.getKey())) {
				line = ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + ".set(\"order_no\", " + entry.getKey() + ");" ;
				method.addBodyLine(line);
			}else {
				line = ObligateInnerVariableName.OBLIGATE_ORDER_FIELDS + ".put(\"" + entry.getKey() + "\", " + entry.getKey() + ");";
				method.addBodyLine(line);
			}
		}
		//存储控制域
		line = ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + ".set(\"fields\", JSONObject.toJSONString(" + ObligateInnerVariableName.OBLIGATE_ORDER_FIELDS + "));" ;
		method.addBodyLine(line);
		line = ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + ".save();";
		method.addBodyLine(line);
		method.addBodyLine("");
		
		//处理响应字段提取
		addDrawRespFieldExpression(importedTypes, method.getBodyLines());
		method.addBodyLine("");
		
		//控制：网关交易模拟支付页面，只需将字段控制规则中的【订单号，交易金额，支付逻辑（挡板模拟通知地址,挡板自动计算）】set到attr中，然后渲染惠付挡板模拟统一网关交易收银台即可
		line = "setAttr(\"orderNo\", " + ObligateInnerVariableName.OBLIGATE_ORDER_NO + ");";
		method.addBodyLine(line);
		Set<String> currentFields = getIntrospectedTrade().getCurrentControlField();
		if(currentFields != null) {
			for (String fieldId : currentFields) {
				List<IntrospectedFieldControl> fieldControls = getIntrospectedChannel().getActualIntrospectedFieldControl(fieldId);
				if(fieldControls == null) {
					throw new RuntimeException("Miss actual field control define.");
				}
				for (IntrospectedFieldControl fieldControl : fieldControls) {
					for (Rules rule : fieldControl.getFieldControlRule()) {
						FieldControlRules controlRule = (FieldControlRules) rule;
						line = "setAttr(\"" + controlRule.getName() + "\", " + controlRule.getName() + ");";
						method.addBodyLine(line);
					}
				}
			}
		}
		//default url http://localhost:8082/simu/
		line = "setAttr(\"pay_url\", \"http://localhost:8082/simu/" + getIntrospectedTrade().getTradeNotificationMethodName() + "\");";
		method.addBodyLine(line);
		
		method.addBodyLine("");
		method.addBodyLine("render(\"/common/cashier.html\");");
		topClass.addImportTypes(importedTypes);
		topClass.addMethod(method);
	}
	
	public static void main(String[] args) {
		TopLevelClass topClass = new TopLevelClass("com.hundsun.epay.autosimu.UnionPayGateWaySimu");
		topClass.setVisibility(JavaVisibility.PUBLIC);
		topClass.setSuperClass(new FullyQualifiedJavaType("com.hundsun.epay.simu.controller.BaseController"));
		new GatewayTradeGenerator().addTradeImplementsElements(topClass);
		System.out.println(topClass.getFormattedContent());
	}
}
