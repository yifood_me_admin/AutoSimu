package com.hundsun.epay.autosimu.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hundsun.epay.autosimu.enums.CommunicationMethod;
import com.hundsun.epay.autosimu.enums.TradeType;
import com.hundsun.epay.autosimu.utils.StringUtility;

public class Trade {
	private String id;
	private CommunicationMethod commMethod;
	private TradeType type;
	private String methodName;
	private String notifyName;
	
	private String notify;    //异步通知交易id
	
	private Boolean sync;   //异步通知调用异步标识（true异步，false同步）
	
	private Map<String, ControllerField> cfields;
	private ControllerResp defaultCResp;
	private List<ControllerResp> cresps;
	
	public Trade() {
		super();
		cfields = new HashMap<String, ControllerField>();
		cresps = new ArrayList<ControllerResp>();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public TradeType getType() {
		return type;
	}
	public void setType(TradeType type) {
		this.type = type;
	}

	public CommunicationMethod getCommMethod() {
		return commMethod;
	}

	public void setCommMethod(CommunicationMethod commMethod) {
		this.commMethod = commMethod;
	}
	
	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getNotifyName() {
		return notifyName;
	}

	public void setNotifyName(String notifyName) {
		this.notifyName = notifyName;
	}
	
	public void addCField(String id, ControllerField field){
		cfields.put(id, field);
	}
	
	public void addCResp(ControllerResp cresp){
		cresps.add(cresp);
	}
	
	/**
	 * @return the defaultCResp
	 */
	public ControllerResp getDefaultCResp() {
		return defaultCResp;
	}

	/**
	 * @param defaultCResp the defaultCResp to set
	 */
	public void setDefaultCResp(ControllerResp defaultCResp) {
		this.defaultCResp = defaultCResp;
	}

	/**
	 * @return the cfields
	 */
	public Map<String, ControllerField> getCfields() {
		return cfields;
	}

	/**
	 * @return the cresps
	 */
	public List<ControllerResp> getCresps() {
		return cresps;
	}
	
	/**
	 * @return the notify
	 */
	public String getNotify() {
		return notify;
	}

	/**
	 * @param notify the notify to set
	 */
	public void setNotify(String notify) {
		this.notify = notify;
	}
	
	/**
	 * @return the sync
	 */
	public Boolean getSync() {
		return sync;
	}

	/**
	 * @param sync the sync to set
	 */
	public void setSync(Boolean sync) {
		this.sync = sync;
	}

	/**
	 * 自我校验
	 * @param errors
	 * @param hasComm
	 */
	public void validate(List<String> errors, boolean hasComm){
		if(!StringUtility.stringHasValue(id)){
			errors.add("\"id\" is required in a context");
		}
		if(!(hasComm || commMethod != null)){
			errors.add("Need at least one communication method configuration.");
		}
		if(type == null){
			errors.add("Trade type is required in a trade.");
		}
		if(defaultCResp == null){
			errors.add("Need one default resp template");
		}
		
		/*校验控制字段*/
		for (ControllerField field : cfields.values()) {
			field.validate(errors);
		}
		/*校验默认响应控制*/
		defaultCResp.validate(errors);
		/*校验响应控制*/
		for (ControllerResp controllerResp : cresps) {
			controllerResp.validate(errors);
		}
		
		if(type.equals(TradeType.NOTIFY) && sync == null){
			errors.add("Notify trade must appointed the sync or not");
		}
	}
}
