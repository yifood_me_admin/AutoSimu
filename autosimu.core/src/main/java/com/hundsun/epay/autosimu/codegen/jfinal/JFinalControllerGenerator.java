package com.hundsun.epay.autosimu.codegen.jfinal;

import java.util.ArrayList;
import java.util.List;

import com.hundsun.epay.autosimu.api.IntrospectedTrade;
import com.hundsun.epay.autosimu.api.dom.java.CompilationUnit;
import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.api.dom.java.JavaVisibility;
import com.hundsun.epay.autosimu.api.dom.java.TopLevelClass;
import com.hundsun.epay.autosimu.codegen.AbstractControllerGenerator;
import com.hundsun.epay.autosimu.codegen.AbstractTradeGenerator;
import com.hundsun.epay.autosimu.codegen.jfinal.trade.CommonSingleTradeGenerator;
import com.hundsun.epay.autosimu.codegen.jfinal.trade.GatewayTradeGenerator;
import com.hundsun.epay.autosimu.codegen.jfinal.trade.NotifyTradeGenerator;
import com.hundsun.epay.autosimu.codegen.jfinal.trade.SingleQueryTradeGenerator;
import com.hundsun.epay.autosimu.enums.TradeType;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * JFinal Controller生成器
 * @author hspcadmin
 *
 */
public class JFinalControllerGenerator extends AbstractControllerGenerator {
	
	@Override
	public List<CompilationUnit> getCompilationUnits() {
		//创建controller类
		String topClassType = introspectedChannel.getControllerImplType();
		TopLevelClass topClass = new TopLevelClass(topClassType);
		topClass.setVisibility(JavaVisibility.PUBLIC);
		String superType = getSuperController();
		if(StringUtility.stringHasValue(superType)){
			FullyQualifiedJavaType suType = new FullyQualifiedJavaType(superType);
			topClass.setSuperClass(suType);
			topClass.addImportType(suType);
		}
		
		List<IntrospectedTrade> trades = introspectedChannel.getTrades();
		for (IntrospectedTrade introspectedTrade : trades) {
			AbstractTradeGenerator generator = calculateAbstractTradeGenerator(introspectedTrade);
			initializeAndExecuteGenerator(generator, introspectedTrade, topClass);
		}
		List<CompilationUnit> answer = new ArrayList<CompilationUnit>();
		answer.add(topClass);
		return answer;
	}

	@Override
	public String getSuperController() {
		return "com.hundsun.epay.simu.controller.BaseController";
	}
	
	protected AbstractTradeGenerator calculateAbstractTradeGenerator(IntrospectedTrade trade){
		if(TradeType.GATEWAY == trade.getTradeType()){
			return new GatewayTradeGenerator();
		}else if(TradeType.SINGLE_QUERY == trade.getTradeType()){
			return new SingleQueryTradeGenerator();
		}else if(TradeType.NOTIFY == trade.getTradeType()){
			return new NotifyTradeGenerator();
		}else if(TradeType.SINGLE_TRADE == trade.getTradeType()){
			return new CommonSingleTradeGenerator();
		}else{
			return null;
		}
	}

	protected void initializeAndExecuteGenerator(AbstractTradeGenerator generator, IntrospectedTrade trade ,TopLevelClass topClass){
		generator.setIntrospectedChannel(introspectedChannel);
		generator.setPayCorp(payCorp);
		generator.setWarnings(warnings);
		generator.setIntrospectedTrade(trade);
		generator.addTradeImplementsElements(topClass);
	}
}
