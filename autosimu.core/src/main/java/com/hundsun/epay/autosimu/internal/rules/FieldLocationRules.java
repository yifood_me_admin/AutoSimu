package com.hundsun.epay.autosimu.internal.rules;

/**
 * 字段定位规则
 * @author Clown
 *
 */
public class FieldLocationRules extends Rules {

	private FieldLocationRules(){
	}
	
	public FieldLocationRules(String rules) {
		parseRules(rules);
	}

	@Override
	protected Rules getParentInstance() {
		return new FieldLocationRules();
	}
	
	
}
