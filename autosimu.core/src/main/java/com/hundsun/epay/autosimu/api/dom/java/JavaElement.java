package com.hundsun.epay.autosimu.api.dom.java;

import java.util.ArrayList;
import java.util.List;

import com.hundsun.epay.autosimu.api.dom.OutputUtility;

/**
 * Java元素
 * @author clown
 *
 */
public class JavaElement {
	private List<String> javaDocLines;   //注释
	private List<String> annotations;   //注解
	private boolean isStatic;   //静态标识
	private boolean isFinal;   //常量标识
	private JavaVisibility visibility = JavaVisibility.DEFAULT;   //可见性
	public JavaElement() {
		super();
		javaDocLines = new ArrayList<String>();
		annotations = new ArrayList<String>();
	}
	public JavaElement(JavaElement ele){
		this();
		this.javaDocLines.addAll(ele.getJavaDocLines());
		this.annotations.addAll(ele.getAnnotations());
		this.isStatic = ele.isStatic();
		this.isFinal = ele.isFinal();
		this.visibility = ele.getVisibility();
	}
	public boolean isStatic() {
		return isStatic;
	}
	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}
	public boolean isFinal() {
		return isFinal;
	}
	public void setFinal(boolean isFinal) {
		this.isFinal = isFinal;
	}
	public JavaVisibility getVisibility() {
		return visibility;
	}
	public void setVisibility(JavaVisibility visibility) {
		this.visibility = visibility;
	}
	public List<String> getJavaDocLines() {
		return javaDocLines;
	}
	public List<String> getAnnotations() {
		return annotations;
	}
	
	public void addJavaDocLines(String javaDocLine){
		this.javaDocLines.add(javaDocLine);
	}
	
	public void addAnnotation(String annotation){
		this.annotations.add(annotation);
	}
	
	public void addSuppressWraningTypeAnnotation(){
		this.annotations.add("@SuppressWranings(\"unchecked\")");
	}
	
	public void addFormattedJavaDocLines(StringBuilder sb, int indentLevel){
		for (String docLine : javaDocLines) {
			OutputUtility.javaIndent(sb, indentLevel);
			sb.append(docLine);
			OutputUtility.newLine(sb);
		}
	}
	
	public void addFormattedAnnotation(StringBuilder sb, int indentLevel){
		for (String annotation : annotations) {
			OutputUtility.javaIndent(sb, indentLevel);
			sb.append(annotation);
			OutputUtility.newLine(sb);
		}
	}
}
