package com.hundsun.epay.autosimu.api.dom.java;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hundsun.epay.autosimu.api.dom.OutputUtility;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 接口
 * @author Clown
 * @Date 2018-02-05
 */
public class Interface extends InnerInterface implements CompilationUnit {
	/**文件注释*/
	private List<String> fileCommentLines;
	/**类型导入*/
	private Set<FullyQualifiedJavaType> importedTypes;
	/**静态导入*/
	private Set<String> staticImportedTypes;
	
	public Interface(FullyQualifiedJavaType type) {
		super(type);
		this.fileCommentLines = new ArrayList<String>();
		this.importedTypes = new HashSet<FullyQualifiedJavaType>();
		this.staticImportedTypes = new HashSet<String>();
	}
	
	public Interface(String type){
		this(new FullyQualifiedJavaType(type));
	}

	@Override
	public Set<FullyQualifiedJavaType> getImportedType() {
		return importedTypes;
	}

	@Override
	public void addImportType(FullyQualifiedJavaType type) {
		if(type != null 
				&& type.isExplicityImported()
				&& !type.getPackageName().equals(this.getType().getPackageName())
				&& !type.getShortName().equals(getType().getShortName())){
			this.importedTypes.add(type);
		}
	}

	@Override
	public void addImportTypes(Set<FullyQualifiedJavaType> types) {
		if(types != null){
			for (FullyQualifiedJavaType fullyQualifiedJavaType : types) {
				this.addImportType(fullyQualifiedJavaType);
			}
		}
	}

	@Override
	public Set<String> getStaticImports() {
		return staticImportedTypes;
	}

	@Override
	public void addStaticImport(String importEle) {
		this.staticImportedTypes.add(importEle);
	}

	@Override
	public void addStaticImports(Set<String> importEles) {
		this.staticImportedTypes.addAll(importEles);
	}


	@Override
	public boolean isJavaInteface() {
		return true;
	}

	@Override
	public boolean isJavaEnumeration() {
		return false;
	}

	@Override
	public void addFileCommentLines(String line) {
		this.fileCommentLines.add(line);
	}

	@Override
	public List<String> getFileCommentLines() {
		return fileCommentLines;
	}

	@Override
	public String getFormattedContent() {
		StringBuilder sb = new StringBuilder();
		
		//文件注释
		for (String line : fileCommentLines) {
			sb.append(line);
			OutputUtility.newLine(sb);
		}
		if(StringUtility.stringHasValue(getType().getPackageName())){
			sb.append("package ");
			sb.append(this.getType().getPackageName());
			sb.append(";");
			OutputUtility.newLine(sb);
			OutputUtility.newLine(sb);
		}
		
		Set<String> typeStrings = OutputUtility.calculateImportTypes(importedTypes);
		for (String typeString : typeStrings) {
			sb.append(typeString);
			OutputUtility.newLine(sb);
		}
		if(!typeStrings.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		for (String typeString : staticImportedTypes) {
			sb.append("import static ");
			sb.append(typeString);
			sb.append(";");
			OutputUtility.newLine(sb);
		}
		
		if(!staticImportedTypes.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		sb.append(super.getFormattedContent(0, this));
		return sb.toString();
	}
}
