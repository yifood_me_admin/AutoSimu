package com.hundsun.epay.autosimu.codegen.jfinal.trade;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.api.dom.java.Method;
import com.hundsun.epay.autosimu.api.dom.java.TopLevelClass;
import com.hundsun.epay.autosimu.internal.ObligateInnerVariableName;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 通用单笔交易生成器
 * @author Clown
 *
 */
public class CommonSingleTradeGenerator extends JFinalAbstractTradeGenerator {

	@Override
	public void addTradeImplementsElements(TopLevelClass topClass) {
		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
		Method method = getMethodShell();
		//读取订单号，透传字段参数
		Map<String, String> params = getAbstractRequestDataDrawer().addDrawRequestDataExpression(importedTypes, this, method.getBodyLines());
		
		importedTypes.add(new FullyQualifiedJavaType("com.hundsun.epay.simu.model.Orders"));
//		importedTypes.add(new FullyQualifiedJavaType("com.hundsun.epay.simu.service.OrderService"));
		importedTypes.add(new FullyQualifiedJavaType("java.util.Map"));
		importedTypes.add(new FullyQualifiedJavaType("java.util.HashMap"));
		importedTypes.add(new FullyQualifiedJavaType("com.alibaba.fastjson.JSONObject"));
		
		method.addBodyLine("");
		
		//存库表
		String line = "Orders " + ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + " = new Orders();";
		method.addBodyLine(line);
		line = "Map<String, String> " + ObligateInnerVariableName.OBLIGATE_ORDER_FIELDS + " = new HashMap<String, String>();";
		method.addBodyLine(line);
		for (Entry<String, String> entry : params.entrySet()) {
			if(ObligateInnerVariableName.OBLIGATE_ORDER_NO.equals(entry.getKey())) {
				line = ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + ".set(\"order_no\", " + entry.getKey() + ");" ;
				method.addBodyLine(line);
			}else {
				line = ObligateInnerVariableName.OBLIGATE_ORDER_FIELDS + ".put(\"" + entry.getKey() + "\", " + entry.getKey() + ");";
				method.addBodyLine(line);
			}
		}
		//存储控制域
		line = ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + ".set(\"fields\", JSONObject.toJSONString(" + ObligateInnerVariableName.OBLIGATE_ORDER_FIELDS + "));" ;
		method.addBodyLine(line);
		line = ObligateInnerVariableName.OBLIGATE_ORDER_OBJECT + ".save();";
		method.addBodyLine(line);
		method.addBodyLine("");
		
		//处理响应字段提取
		addDrawRespFieldExpression(importedTypes, method.getBodyLines());
		method.addBodyLine("");
		
		//处理通知
		if(StringUtility.stringHasValue(getIntrospectedTrade().getTradeNotificationMethodName())){
			line = getIntrospectedTrade().getTradeNotificationMethodName() + "(" + ObligateInnerVariableName.OBLIGATE_ORDER_NO + ");";
			method.addBodyLine(line);
		}
		//处理响应替换
		assembleCheckSingleResp(method);
		method.addBodyLine("");
		
		//响应报文
		method.addBodyLine(renderText(ObligateInnerVariableName.OBLIGATE_TEMPLATE_RESPMSG));
		
		topClass.addImportTypes(importedTypes);
		topClass.addMethod(method);
	}

}
