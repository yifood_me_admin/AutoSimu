package com.hundsun.epay.autosimu.enums;

public enum ReservedWord {
	OVERTIME("OVERTIME"),
	EXCETPION("EXCETPION"),
	;
	private String reservedWord;

	private ReservedWord(String reservedWord) {
		this.reservedWord = reservedWord;
	}
	
	public static ReservedWord getWrodByCode(String code){
		for (ReservedWord word : values()) {
			if(word.reservedWord.equals(code)){
				return word;
			}
		}
		return null;
	}

	/**
	 * @return the reservedWord
	 */
	public String getReservedWord() {
		return reservedWord;
	}
	
}
