package com.hundsun.epay.autosimu.codegen.jfinal.drawer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hundsun.epay.autosimu.api.IntrospectedFieldControl;
import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.codegen.AbstractJSONRequestDataDrawer;
import com.hundsun.epay.autosimu.codegen.AbstractTradeGenerator;
import com.hundsun.epay.autosimu.internal.ObligateInnerVariableName;
import com.hundsun.epay.autosimu.internal.rules.Rules;

public class JFinalJSONRequestDataDrawer extends
		AbstractJSONRequestDataDrawer {

	@Override
	public Map<String, String> addDrawRequestDataExpression(
			Set<FullyQualifiedJavaType> importTypes,
			AbstractTradeGenerator tradeGenerator, List<String> bodies) {
		importTypes.add(new FullyQualifiedJavaType("com.jfinal.kit.HttpKit"));
		importTypes.add(new FullyQualifiedJavaType("com.alibaba.fastjson.JSONObject"));
		
		//请求数据读取
		StringBuilder builder = new StringBuilder();
		builder.append("String ")
			.append(ObligateInnerVariableName.OBLIGATE_REQ_DATA)
			.append(" = ")
			.append("HttpKit.readData(getRequest());");
		bodies.add(builder.toString());
		builder.setLength(0);
		
		//请求数据解析
		builder.append("JSONObject ")
			.append(ObligateInnerVariableName.OBLIGATE_REQ_OBJECT)
			.append(" = ")
			.append("JSONObject.parseObject(")
			.append(ObligateInnerVariableName.OBLIGATE_REQ_DATA)
			.append(");");
		bodies.add(builder.toString());
		builder.setLength(0);
		
		Set<IntrospectedFieldControl> keyRules = getParamKeyRules(tradeGenerator);
		Map<String, String> requestParamNameMap = new HashMap<String, String>();
		//处理数据获取
		for (IntrospectedFieldControl introspectedFieldControl : keyRules) {
			drawKeyRequestDataFor(introspectedFieldControl, requestParamNameMap, bodies);
		}
		return requestParamNameMap;
	}
	
	/**
	 * 提取控制key的请求数据，该部分处理过滤掉key定位路径中存在数组且为重复提取的情况  isRepeat = true
	 * @param keyRule
	 * @param keyPaths
	 * @param builder
	 */
	private void drawKeyRequestDataFor(IntrospectedFieldControl introspectedFieldControl, Map<String, String> requestParamNameMap, List<String> bodies){
		StringBuilder builder = new StringBuilder();
		String innerName = introspectedFieldControl.getInnerVaribaleName();
		if(requestParamNameMap.containsKey(innerName)){//重复提取
			return;
		}
		requestParamNameMap.put(innerName, null);
		builder.append("String ")
			.append(innerName)
			.append(" = ")
			.append(ObligateInnerVariableName.OBLIGATE_REQ_OBJECT);
		boolean repeat = writeDrawKeyRequestDataExpression(introspectedFieldControl.getFieldControlKeyRule(), builder, true);
		//repeat结果为false为repeaet路径
		if(!repeat){
			return;
		}
		builder.append(";");
		bodies.add(builder.toString());
	}
	
	/**
	 * 递归提取json对象中的元素
	 * @param keyRule
	 * @param builder
	 * @return
	 */
	private boolean writeDrawKeyRequestDataExpression(Rules keyRule, StringBuilder builder, boolean tail){
		//控制key为重复提取的key为控制明细部分字段，不提取，过滤
		if(keyRule.isRepeat()){
			return false;   //返回null 则表示需过滤
		}
		if(keyRule.getParent() != null){
			boolean isRepeat = writeDrawKeyRequestDataExpression(keyRule.getParent(), builder, false);
			if(!isRepeat){//递归父节点得到的结果为空，表明父节点中为repeat
				return false;
			}
		}
		//tail:是否是json路径中的最后一个
		//isInArray:是否在数组列表中指定了下标
		if(keyRule.isInArray()){  //为数组中，必有下表index
			builder.append(".getJSONArray(\"").append(keyRule.getName()).append("\")");
			if(tail){
				builder.append(".getString(");
			}else{
				builder.append(".getJSONObject(");
			}
			builder.append(keyRule.getIndex()).append(")");
		}else{
			if(tail){
				builder.append(".getString(\"");
			}else{
				builder.append(".getJSONObject(\"");
			}
			builder.append(keyRule.getName()).append("\")");
		}
		return true;
	}
}
