package com.hundsun.epay.autosimu.exception;

import java.util.List;

public class InvalidConfigurationException extends Exception {
	private static final long serialVersionUID = 5666034162542695810L;

	private List<String> errors;
	
	public InvalidConfigurationException(List<String> errors) {
		super();
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}

	@Override
	public String getMessage() {
		if(errors != null && !errors.isEmpty()){
			return errors.get(0);
		}
		return super.getMessage();
	}
	
	
}
