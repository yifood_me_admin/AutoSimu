package com.hundsun.epay.autosimu.api.dom.java;

import com.hundsun.epay.autosimu.api.dom.OutputUtility;

public class Field extends JavaElement {
	private FullyQualifiedJavaType type;    //类型
	private String name;   //标识符
	private String initString;   //初始化字符串
	private boolean isTransient;    //transient
	private boolean isVolatile;   //volatile
	
	public Field(FullyQualifiedJavaType type, String name) {
		super();
		this.type = type;
		this.name = name;
	}

	public FullyQualifiedJavaType getType() {
		return type;
	}

	public void setType(FullyQualifiedJavaType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitString() {
		return initString;
	}

	public void setInitString(String initString) {
		this.initString = initString;
	}

	public boolean isTransient() {
		return isTransient;
	}

	public void setTransient(boolean isTransient) {
		this.isTransient = isTransient;
	}

	public boolean isVolatile() {
		return isVolatile;
	}

	public void setVolatile(boolean isVolatile) {
		this.isVolatile = isVolatile;
	}
	
	public String getFormattedContent(int indentLevel, CompilationUnit compileUnit){
		StringBuilder sb = new StringBuilder();
		//添加注释
		addFormattedJavaDocLines(sb, indentLevel);
		//添加注解
		addFormattedAnnotation(sb, indentLevel);
		//添加修饰符
		OutputUtility.javaIndent(sb, indentLevel);
		sb.append(getVisibility().getDesc());
		if(isStatic()){
			sb.append("static ");
		}
		if(isFinal()){
			sb.append("final ");
		}
		if(isTransient()){
			sb.append("transient ");
		}
		if(isVolatile()){
			sb.append("volatile ");
		}
		//添加类型名
		sb.append(JavaDomUtils.calculateTypeName(compileUnit, type));
		sb.append(" ");
		//添加属性名
		sb.append(name);
		//添加初始化
		if(!(initString == null || initString.isEmpty())){
			sb.append(" = ");
			sb.append(initString);
		}
		sb.append(";");
		return sb.toString();
	}
}
