package com.hundsun.epay.autosimu.api.dom.java;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.hundsun.epay.autosimu.api.dom.OutputUtility;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 枚举
 * @author Clown
 * @Date 2018-02-05
 */
public class TopLevelEnumeration extends InnerEnumeration implements
		CompilationUnit {
	/**文件注释*/
	private List<String> fileCommentLines;
	/**类型导入*/
	private Set<FullyQualifiedJavaType> importedTypes;
	/**静态导入*/
	private Set<String> staticImportedTypes;
	
	public TopLevelEnumeration(FullyQualifiedJavaType type) {
		super(type);
		this.fileCommentLines = new ArrayList<String>();
		this.importedTypes = new TreeSet<FullyQualifiedJavaType>();
		this.staticImportedTypes = new TreeSet<String>();
	}
	
	public TopLevelEnumeration(String type) {
		this(new FullyQualifiedJavaType(type));
	}

	@Override
	public Set<FullyQualifiedJavaType> getImportedType() {
		return importedTypes;
	}
	
	public void addImportType(String type){
		this.addImportType(new FullyQualifiedJavaType(type));
	}

	@Override
	public void addImportType(FullyQualifiedJavaType type) {
		if(type != null  //类型不为空
				&& type.isExplicityImported()   //不为java.lang包下的类
				&& !type.getPackageName().equals(this.getType().getPackageName())   //不在同一个包下
				&& !type.getShortName().equals(this.getType().getShortName())){   //类名与本类类名不同
			this.importedTypes.add(type);
		}
	}

	@Override
	public void addImportTypes(Set<FullyQualifiedJavaType> types) {
		if(types != null){
			for (FullyQualifiedJavaType type : types) {
				this.addImportType(type);
			}
		}
	}

	@Override
	public Set<String> getStaticImports() {
		return staticImportedTypes;
	}

	@Override
	public void addStaticImport(String importEle) {
		this.staticImportedTypes.add(importEle);
	}

	@Override
	public void addStaticImports(Set<String> importEles) {
		this.staticImportedTypes.addAll(importEles);
	}

	@Override
	public boolean isJavaInteface() {
		return false;
	}

	@Override
	public boolean isJavaEnumeration() {
		return false;
	}

	@Override
	public void addFileCommentLines(String line) {
		this.fileCommentLines.add(line);
	}

	@Override
	public List<String> getFileCommentLines() {
		return fileCommentLines;
	}

	@Override
	public String getFormattedContent() {
		StringBuilder sb =  new StringBuilder();
		
		//文件注释
		for (String line : fileCommentLines) {
			sb.append(line);
			OutputUtility.newLine(sb);
		}
		
		//包定义
		if(StringUtility.stringHasValue(getType().getPackageName())){
			sb.append("package ");
			sb.append(getType().getPackageName());
			sb.append(";");
			OutputUtility.newLine(sb);
			OutputUtility.newLine(sb);
		}
		
		//import
		Set<String> importTypeStrings = OutputUtility.calculateImportTypes(importedTypes);
		for (String typeString : importTypeStrings) {
			sb.append(typeString);
			OutputUtility.newLine(sb);
		}
		
		if(!importTypeStrings.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//import static
		for (String type : staticImportedTypes) {
			sb.append("import static ");
			sb.append(type);
			sb.append(";");
			OutputUtility.newLine(sb);
		}
		
		if(!staticImportedTypes.isEmpty()){
			OutputUtility.newLine(sb);
		}
		
		//枚举
		sb.append(super.getFormattedContent(0, this));
		return sb.toString();
	}

	@Override
	public FullyQualifiedJavaType getSuperClass() {
		throw new UnsupportedOperationException("Enumerations do not have super classes");
	}
}
