package com.hundsun.epay.autosimu.codegen;

import com.hundsun.epay.autosimu.api.IntrospectedFieldControl;
import com.hundsun.epay.autosimu.internal.rules.Rules;

/**
 * key-value 请求数据提取器
 * @author clown
 *
 */
public abstract class AbstractKeyValueRequestDataDrawer extends AbstractRequestDataDrawer {
	protected String calculateKeyName(IntrospectedFieldControl introspectedRule) {
//		return introspectedRule.getFieldControlId();
		return calculateKeyName(introspectedRule.getFieldControlKeyRule());
	}
	
	private String calculateKeyName(Rules rule) {
		if(rule.getParent() != null) {
			return calculateKeyName(rule.getParent());
		}else {
			return rule.getName();
		}
	}
}
