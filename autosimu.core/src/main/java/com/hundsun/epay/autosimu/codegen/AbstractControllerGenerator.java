package com.hundsun.epay.autosimu.codegen;

/**
 * 控制器生成器
 * @author Clown
 *
 */
public abstract class AbstractControllerGenerator extends
		AbstractJavaGenerator {
	public abstract String getSuperController();
}
