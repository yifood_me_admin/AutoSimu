package com.hundsun.epay.autosimu.internal.parse;

/**
 * 运算解析器
 * @author Clown
 *
 */
public abstract class OperatorParser {
	private String firstVar;
	private String secondVar;
	
	public abstract void parseExpression(String expression);
	
	/**
	 * 格式化运算表达式
	 * @return
	 */
	public abstract String format();
	/**
	 * @return the firstVar
	 */
	public String getFirstVar() {
		return firstVar;
	}
	/**
	 * @param firstVar the firstVar to set
	 */
	public void setFirstVar(String firstVar) {
		this.firstVar = firstVar;
	}
	/**
	 * @return the secondVar
	 */
	public String getSecondVar() {
		return secondVar;
	}
	/**
	 * @param secondVar the secondVar to set
	 */
	public void setSecondVar(String secondVar) {
		this.secondVar = secondVar;
	}
	
	
}
