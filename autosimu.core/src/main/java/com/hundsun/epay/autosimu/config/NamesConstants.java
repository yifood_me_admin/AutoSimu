package com.hundsun.epay.autosimu.config;

public class NamesConstants {
	//节点名
	public static final String ROOT_NODE_NAME = "config";
	public static final String NODE_PAY_CORPORATION = "payCorp";
	public static final String NODE_PAY_CHANNEL = "channel";
	public static final String NODE_PAY_TRADE = "trade";
	public static final String NODE_CONTROLLER_CFIELDS = "cfields";
	public static final String NODE_CONTROLLER_CFIELD = "cfield";
	public static final String NODE_CONTROLLER_CRESPS = "cresps";
	public static final String NODE_CONTROLLER_CRESP = "cresp";
	
	
	//属性名称
	public static final String ATTRIBUTE_ID = "id";
	public static final String ATTRIBUTE_CLASS_NAME = "className";
	public static final String ATTRIBUTE_COMMUNICATION_METHOD = "comm-method";
	public static final String ATTRIBUTE_TYPE = "type";
	public static final String ATTRIBUTE_PACKAGE_NAME = "packageName";
	public static final String ATTRIBUTE_METHON_NAME = "methodName";
	public static final String ATTRIBUTE_NOTIFY_NAME = "notifyName";
	public static final String ATTRIBUTE_NOTIFY = "notify";
	public static final String ATTRIBUTE_KEY = "key";
	public static final String ATTRIBUTE_RULE = "rule";
	public static final String ATTRIBUTE_CONTROL = "control";
	public static final String ATTRIBUTE_TEMPLATE = "template";
	public static final String ATTRIBUTE_REF = "ref";
	public static final String ATTRIBUTE_SEPARATOR = "separator";
	public static final String ATTRIBUTE_IMPORT = "import";
	public static final String ATTRIBUTE_SYNC = "sync";
}
