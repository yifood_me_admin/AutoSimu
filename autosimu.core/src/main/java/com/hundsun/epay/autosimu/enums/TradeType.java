package com.hundsun.epay.autosimu.enums;

public enum TradeType {
	GATEWAY("gateway"),
	SINGLE_QUERY("singleQuery"),
	NOTIFY("notify"),
	SINGLE_TRADE("singleTrade"),
	;
	private String type;

	private TradeType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	public static TradeType getTypeByCode(String code){
//		if(StringUtility.isBlank(code)){
//			return null;
//		}
		for (TradeType type : values()) {
			if(type.getType().equalsIgnoreCase(code)){
				return type;
			}
		}
		throw new RuntimeException("Invalid type.");
	}
}
