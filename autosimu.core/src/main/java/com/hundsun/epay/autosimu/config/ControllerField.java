package com.hundsun.epay.autosimu.config;

import java.util.List;

import com.hundsun.epay.autosimu.enums.ControllerFieldType;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 控制字段配置
 * @author clown
 *
 */
public class ControllerField {
	/**控制域编号*/
	private String id;
	/**控制类型*/
	private ControllerFieldType type;
	/**
	 * 控制字段名称及定位规则
	 * <li>#:子节点标识
	 * <li>[]:数组循环标识
	 * <li>[index]:指定数组中第index行记录
	 * 
	 * <br>示例：
	 * <li>1. orderNo:表示请求报文中的orderNo字段，取第一个
	 * <li>2. data#summary:表示data的子节点summary
	 * <li>3. details#summary[0]:表示details内的第一行记录的summary
	 * <li>4. details#card_no[]:表示details内每一行记录的detail_no
	 * <li>5. #orderNo:表示最外层的orderNo字段
	 * */
	private String key;
	/**
	 * 控制规则定义：
	 * <li>保留字：OVERTIME,EXCEPTION
	 * <li>关键字：{}外的字符部分，不含[]，此关键字对应于响应报文，进行同名替换（响应中用#包围着的关键字）
	 * <li>[]:循环处理标识，指定循环处理包含[]修饰的关键字的外层节点
	 * <li>{}：控制规则定义，分两部分，以:分隔，
	 * <br>&nbsp;&nbsp;&nbsp;&nbsp;	   :之前定义字符截取，格式为：[-]digit1[,digit2] digit1标识截取下表，负数表示从最后开始，digit2表示长度
	 * <br>&nbsp;&nbsp;&nbsp;&nbsp;	   :之后表示控制特定规则： 多个规则以,分隔，每个规则格式：req=resp:req表示请求传值，取等校验，resp表示响应内容
	 * <br>&nbsp;&nbsp;&nbsp;&nbsp;    使用独立的单引号转义保留字
	 * </li>
	 */
	private String rule;
	/**
	 * 控制字段引用：
	 * 格式：tradeId.filedId1[,fieldId2]
	 */
	private String refField;
	/**
	 * 当前交易是否控制标识
	 */
	private boolean control;
	
	private String separator;
	
	public ControllerField() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ControllerFieldType getType() {
		return type;
	}

	public void setType(ControllerFieldType type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getRefField() {
		return refField;
	}

	public void setRefField(String refField) {
		this.refField = refField;
	}

	public boolean isControl() {
		return control;
	}

	public void setControl(Boolean control) {
		this.control = control;
	}
	
	/**
	 * @return the separator
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * @param separator the separator to set
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}

	/**
	 * 自我校验
	 * @param errors
	 */
	public void validate(List<String> errors){
		if(!StringUtility.stringHasValue(id)){
			errors.add("Need a id for field controller");
		}
		if(type == null){
			errors.add("Need a valid type for field controller");
		}
		if(!ControllerFieldType.REF.equals(type) && !StringUtility.stringHasValue(key)){
			errors.add("Need a key definition for field controller");
		}
		if(ControllerFieldType.CONTROLLER.equals(type) && !StringUtility.stringHasValue(rule)){
			errors.add("Need a rule definition for a field controller which type is CONTROLLER");
		}
		if(separator != null && separator.length() > 1){
			errors.add("The separator can only contain one character.");
		}
		if(ControllerFieldType.REF.equals(type) && !StringUtility.stringHasValue(refField)){
			errors.add("Need a valid reference defination for a REF field.");
		}
	}
	
	
}
