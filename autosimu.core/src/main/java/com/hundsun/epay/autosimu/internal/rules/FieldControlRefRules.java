package com.hundsun.epay.autosimu.internal.rules;

import com.hundsun.epay.autosimu.utils.StringUtility;

public class FieldControlRefRules extends Rules {
	protected String tradeId;
	protected String fieldId;
	
	public FieldControlRefRules(String rules, String currentTradeId){
		//引用规则中的id等不支持循环、父子关系解析
		parseFieldControlRefRules(rules, currentTradeId);
	}
	
	protected void parseFieldControlRefRules(String rules, String currentTradeId){
		int leftDot = rules.indexOf(".");
		if(leftDot >= rules.length() - 1){
			throw new RuntimeException("The referenced field id can't be empty.");
		}
		if(leftDot == 0){
			throw new RuntimeException("Can not appointe a empty trade id.");
		}
		
		if(leftDot < 0){
			if(StringUtility.isBlank(currentTradeId)){
				throw new RuntimeException("Need a trade id.");
			}
			this.tradeId = currentTradeId;
			this.fieldId = rules;
		}else{
			this.tradeId = rules.substring(0, leftDot);
			this.fieldId = rules.substring(leftDot + 1);
		}
	}
	
	@Override
	protected Rules getParentInstance() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @return the tradeId
	 */
	public String getTradeId() {
		return tradeId;
	}

	/**
	 * @return the fieldId
	 */
	public String getFieldId() {
		return fieldId;
	}
	
	public String getAbsoluteFieldId(){
		return this.tradeId + "." + this.fieldId;
	}
}
