package com.hundsun.epay.autosimu.codegen.jfinal.trade;

import java.util.Set;
import java.util.TreeSet;

import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.api.dom.java.Method;
import com.hundsun.epay.autosimu.api.dom.java.TopLevelClass;
import com.hundsun.epay.autosimu.internal.ObligateInnerVariableName;

/**
 * 单笔订单查询生成器
 * @author clown
 *
 */
public class SingleQueryTradeGenerator extends JFinalAbstractTradeGenerator {

	@Override
	public void addTradeImplementsElements(TopLevelClass topClass) {
		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
		Method method = getMethodShell();
		//读取订单号，透传字段参数
		getAbstractRequestDataDrawer().addDrawRequestDataExpression(importedTypes, this, method.getBodyLines());
	
		importedTypes.add(new FullyQualifiedJavaType("com.hundsun.epay.simu.model.Orders"));
//		importedTypes.add(new FullyQualifiedJavaType("com.hundsun.epay.simu.service.OrderService"));
		importedTypes.add(new FullyQualifiedJavaType("com.alibaba.fastjson.JSONObject"));
		
		String line = "";
		method.addBodyLine(line);
		
		addDrawRequestDataFromDBExpression(importedTypes, method);
		
		method.addBodyLine("");
		
		//处理响应字段提取
		addDrawRespFieldExpression(importedTypes, method.getBodyLines());
		method.addBodyLine("");
		
		//处理响应替换
		assembleCheckSingleResp(method);
		
		method.addBodyLine("");
		method.addBodyLine(renderText(ObligateInnerVariableName.OBLIGATE_TEMPLATE_RESPMSG));
		
		topClass.addImportTypes(importedTypes);
		topClass.addMethod(method);
	}

}
