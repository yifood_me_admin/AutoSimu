package com.hundsun.epay.autosimu.config;

import java.util.ArrayList;
import java.util.List;

import com.hundsun.epay.autosimu.exception.InvalidConfigurationException;

public class Configuration {
	List<PayCorporation> corps;
	
	public Configuration() {
		super();
		corps = new ArrayList<PayCorporation>();
	}

	public void validate() throws InvalidConfigurationException{
		List<String> errors = new ArrayList<String>();
		if(corps.isEmpty()){
			errors.add("At least one configuration element is required");
		}else{
			for (PayCorporation payCorporation : corps) {
				payCorporation.validate(errors);
			}
		}
		
		if(!errors.isEmpty()){
			throw new InvalidConfigurationException(errors);
		}
	}

	public List<PayCorporation> getCorps() {
		return corps;
	}

	public void addCorp(PayCorporation corp) {
		corps.add(corp);
	}
	
	public PayCorporation getCorp(String id){
		for (PayCorporation corp : corps) {
			if(id.equals(corp.getId())){
				return corp;
			}
		}
		return null;
	}
}
