package com.hundsun.epay.autosimu.enums;

public enum CommunicationMethod {
	KEY_VALUE("key-value"),
	JSON("json"),
	XML("xml")
	;
	private String commMethod;

	private CommunicationMethod(String commMethod) {
		this.commMethod = commMethod;
	}
	
	public static CommunicationMethod getMethodByCode(String code){
//		if(code == null){
//			return null;
//		}
		for (CommunicationMethod method : values()) {
			if(method.getCommMethod().equalsIgnoreCase(code)){
				return method;
			}
		}
		throw new RuntimeException("Invalid comm-method.");
	}

	public String getCommMethod() {
		return commMethod;
	}
	
}
