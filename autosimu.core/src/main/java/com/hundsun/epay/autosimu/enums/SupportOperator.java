package com.hundsun.epay.autosimu.enums;

public enum SupportOperator {
//	MODEQUAL("%", "\\%"),    //取模
	EQUAL("=", "\\="),   //取等
	;
	private String operatorChar;
	private String operatorRegex;

	private SupportOperator(String operatorChar, String operatorRegex) {
		this.operatorChar = operatorChar;
		this.operatorRegex = operatorRegex;
	}
	
	/**
	 * 返回查找运算符的正则表达式
	 * @return
	 */
	public static String getFindRegex(){
		StringBuilder sb = new StringBuilder("[");
		for (SupportOperator so : values()) {
			sb.append(so.operatorRegex);
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * @return the operatorRegex
	 */
	public String getOperatorRegex() {
		return operatorRegex;
	}
	
	public static SupportOperator getOperatorByChar(String operatorChar){
		for (SupportOperator so : values()) {
			if(so.operatorChar.equals(operatorChar)){
				return so;
			}
		}
		return null;
	}
}
