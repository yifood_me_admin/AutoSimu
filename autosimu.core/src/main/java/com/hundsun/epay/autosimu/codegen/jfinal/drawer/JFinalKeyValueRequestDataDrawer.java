package com.hundsun.epay.autosimu.codegen.jfinal.drawer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hundsun.epay.autosimu.api.IntrospectedChannel;
import com.hundsun.epay.autosimu.api.IntrospectedFieldControl;
import com.hundsun.epay.autosimu.api.IntrospectedTrade;
import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.codegen.AbstractKeyValueRequestDataDrawer;
import com.hundsun.epay.autosimu.codegen.AbstractTradeGenerator;
import com.hundsun.epay.autosimu.codegen.jfinal.trade.JFinalAbstractTradeGenerator;
import com.hundsun.epay.autosimu.internal.rules.Rules;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * key-value请求参数提取
 * @author clown
 *
 */
public class JFinalKeyValueRequestDataDrawer extends
		AbstractKeyValueRequestDataDrawer {
	@Override
	public Map<String, String> addDrawRequestDataExpression(
			Set<FullyQualifiedJavaType> importTypes,
			AbstractTradeGenerator tradeGenerator, List<String> bodies) {
		importTypes.add(new FullyQualifiedJavaType("java.lang.String"));
		Set<IntrospectedFieldControl> keyRules = getParamKeyRules(tradeGenerator);
		Map<String, String> requestParamNameMap = new HashMap<String, String>();
		for (IntrospectedFieldControl rule : keyRules) {
			String keyName = calculateKeyName(rule);
			String innerVarName = rule.getInnerVaribaleName();
			if(!requestParamNameMap.containsKey(innerVarName)) {//过滤相同的key的不同用处的配置，对应于内部变量名称，不可重复
				bodies.add(new StringBuilder() 
						.append("String ")
						.append(innerVarName)
						.append(" = ")
						.append("getPara(\"")
						.append(keyName)
						.append("\");")
						.toString());
				requestParamNameMap.put(innerVarName, keyName);
			}
		}
		return requestParamNameMap;
	}
}
