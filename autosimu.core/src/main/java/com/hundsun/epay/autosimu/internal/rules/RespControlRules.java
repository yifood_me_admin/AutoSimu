package com.hundsun.epay.autosimu.internal.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hundsun.epay.autosimu.enums.SupportOperator;
import com.hundsun.epay.autosimu.internal.ObjectFactory;
import com.hundsun.epay.autosimu.internal.parse.OperatorParser;

public class RespControlRules extends Rules {
	protected String secondEle;
	protected String value;
	protected OperatorParser parser;
	
	@Override
	protected Rules getParentInstance() {
		throw new UnsupportedOperationException();    //响应规则内部关键字名称不支持父子关系
	}

	public RespControlRules(String rules){
		parseRespRules(rules);
	}
	
	protected void parseRespRules(String rules){
//		int leftEqual = rules.indexOf("=");
//		if(leftEqual <= 0){
//			throw new RuntimeException("The defination need a appointed key.");
//		}
		//取第一个运算符
		Pattern pattern = Pattern.compile(SupportOperator.getFindRegex());
		Matcher matcher = pattern.matcher(rules);
		if(!matcher.find()){
			throw new RuntimeException("The Operator do not Support.");
		}
		String firstOperator = matcher.group();
		SupportOperator so = SupportOperator.getOperatorByChar(firstOperator);
		parser = ObjectFactory.createOperatorParser(so);
		parser.parseExpression(rules);
		this.name = parser.getFirstVar();
		this.secondEle = parser.getSecondVar();
	}

	/**
	 * @return the secondEle
	 */
	public String getSecondEle() {
		return secondEle;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	public String formatExpression(){
		return parser.format();
	}
}
