package com.hundsun.epay.autosimu.codegen.jfinal;

import java.util.ArrayList;
import java.util.List;

import com.hundsun.epay.autosimu.api.GeneratedJavaFile;
import com.hundsun.epay.autosimu.api.IntrospectedChannel;
import com.hundsun.epay.autosimu.api.dom.java.CompilationUnit;
import com.hundsun.epay.autosimu.codegen.AbstractControllerGenerator;
import com.hundsun.epay.autosimu.codegen.AbstractGenerator;

/**
 * 通道的jfinal实现
 * @author Clown
 *
 */
public class JFinalIntrospectedChannelImpl extends IntrospectedChannel {

	private List<AbstractControllerGenerator> controllerGenerators;
	
	public JFinalIntrospectedChannelImpl(){
		controllerGenerators = new ArrayList<AbstractControllerGenerator>();
	}
	
	@Override
	public List<GeneratedJavaFile> getGenerateJavaFiles() {
		for (AbstractControllerGenerator abstractControllerGenerator : controllerGenerators) {
			List<CompilationUnit> units = abstractControllerGenerator.getCompilationUnits();
			for (CompilationUnit compilationUnit : units) {
				System.out.println(compilationUnit.getFormattedContent());
			}
		}
		return new ArrayList<GeneratedJavaFile>();
	}

	@Override
	public void initGenerators(List<String> wrannings) {
		calculateControllerGenerator(wrannings);
	}

	protected void initializeAbstractGenerator(AbstractGenerator abstractGenerator, List<String> warnings){
		if(abstractGenerator == null){
			return;
		}
		abstractGenerator.setPayCorp(payCorpConfig);
		abstractGenerator.setIntrospectedChannel(this);
		abstractGenerator.setWarnings(warnings);
	}
	
	protected void calculateControllerGenerator(List<String> warnnings){
		JFinalControllerGenerator controllerGenerator = new JFinalControllerGenerator();
		initializeAbstractGenerator(controllerGenerator, warnnings);
		controllerGenerators.add(controllerGenerator);
	}
}
