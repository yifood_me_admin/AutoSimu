package com.hundsun.epay.autosimu.config;

import java.util.List;

import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 响应内容控制
 * @author clown
 *
 */
public class ControllerResp {
	/**
	 * 控制规则：
	 * <br>格式：关键字=值1|值2,关键字2=值1|值2
	 * <br>说明：
	 * <li>,分隔同一个规则多个关键字，&&以连接  |分隔同一个规则下一个关键字满足多个值，以||连接
	 * <li>多个规则之间else关系，
	 * <li>rule为空，表示默认配置，必须包含且只能包含一个rule为空的规则
	 */
	private String rule;
	/**
	 * 报文模板
	 */
	private String template;
	
	private String importUrl;
	
	/**
	 * @return the importUrl
	 */
	public String getImportUrl() {
		return importUrl;
	}

	/**
	 * @param importUrl the importUrl to set
	 */
	public void setImportUrl(String importUrl) {
		this.importUrl = importUrl;
	}

	public ControllerResp() {
		super();
	}

	/**
	 * @return the rule
	 */
	public String getRule() {
		return rule;
	}

	/**
	 * @param rule the rule to set
	 */
	public void setRule(String rule) {
		this.rule = rule;
	}

	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}
	
	/**
	 * 自我校验
	 * @param errors
	 */
	public void validate(List<String> errors){
		boolean hasImport = StringUtility.stringHasValue(importUrl);
		boolean hasTemplate = StringUtility.stringHasValue(template);
		if(hasImport && hasTemplate){   //两个属性只能包含一个
			errors.add("Can only contain one attribute of 'template' and 'import'");
		}else if(!(hasImport || hasTemplate)){
			errors.add("Need contain one attribute of 'template' and 'import'");
		}
	}
}
