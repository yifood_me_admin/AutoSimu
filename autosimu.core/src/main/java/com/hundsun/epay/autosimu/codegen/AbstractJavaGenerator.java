package com.hundsun.epay.autosimu.codegen;

import java.util.List;

import com.hundsun.epay.autosimu.api.dom.java.CompilationUnit;

/**
 * Java 生成器
 * @author Clown
 *
 */
public abstract class AbstractJavaGenerator extends AbstractGenerator {
	/**
	 * 编译单元
	 * @return
	 */
	public abstract List<CompilationUnit> getCompilationUnits();
}
