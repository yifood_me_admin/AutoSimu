package com.hundsun.epay.autosimu.codegen.jfinal.trade;

import com.hundsun.epay.autosimu.api.dom.java.FullyQualifiedJavaType;
import com.hundsun.epay.autosimu.api.dom.java.JavaVisibility;
import com.hundsun.epay.autosimu.api.dom.java.Method;
import com.hundsun.epay.autosimu.api.dom.java.Parameter;
import com.hundsun.epay.autosimu.codegen.AbstractRequestDataDrawer;
import com.hundsun.epay.autosimu.codegen.AbstractTradeGenerator;
import com.hundsun.epay.autosimu.codegen.jfinal.drawer.JFinalJSONRequestDataDrawer;
import com.hundsun.epay.autosimu.codegen.jfinal.drawer.JFinalKeyValueRequestDataDrawer;
import com.hundsun.epay.autosimu.codegen.jfinal.drawer.JFinalXMLRequestDataDrawer;
import com.hundsun.epay.autosimu.enums.CommunicationMethod;
import com.hundsun.epay.autosimu.enums.TradeType;
import com.hundsun.epay.autosimu.internal.ObligateInnerVariableName;

/**
 * 交易生成器
 * @author Clown
 *
 */
public abstract class JFinalAbstractTradeGenerator extends AbstractTradeGenerator {
	/**
	 * 返回获取请求对象
	 * @return
	 */
	public String getRequestExpression(){
		return "getRequest()";
	}
	
	/**
	 * 处理响应：以text返回响应
	 * @param text ： 响应信息变量
	 * @return
	 */
	public String renderText(String text){
		return "renderText(" + text + ");";
	}
	
	/**
	 * 获取交易实现方法
	 * @return
	 */
	protected Method getMethodShell(){
		Method method = new Method();
		Boolean notifySync = getIntrospectedTrade().getNotifySync();
		//通知交易类型且非异步调用的通知
		boolean notifyAndNotSync = getIntrospectedTrade().getTradeType().equals(TradeType.NOTIFY) && Boolean.FALSE.equals(notifySync);
		if(notifyAndNotSync){
			method.setVisibility(JavaVisibility.PRIVATE);
		}else{
			method.setVisibility(JavaVisibility.PUBLIC);
		}
		method.setReturnType(null);   //返回类型为void
		method.setName(getIntrospectedTrade().getTradeMethodNameAttribute());
		if(notifyAndNotSync){
			method.addParameter(new Parameter(ObligateInnerVariableName.OBLIGATE_ORDER_NO, FullyQualifiedJavaType.getStringInstance()));
		}
		return method;
	}

	@Override
	protected AbstractRequestDataDrawer getAbstractRequestDataDrawer() {
		CommunicationMethod method = this.getIntrospectedTrade().getTradeCommunicationMethod();
		if(CommunicationMethod.KEY_VALUE == method){
			return new JFinalKeyValueRequestDataDrawer();
		}else if(CommunicationMethod.JSON == method){
			return new JFinalJSONRequestDataDrawer();
		}else if(CommunicationMethod.XML == method){
			return new JFinalXMLRequestDataDrawer();
		}else{
			throw new RuntimeException("通信方法不支持");
		}
	}
	
	
}
