package com.hundsun.epay.autosimu.enums;

public enum ControllerFieldType {
	ORDER_NO("ORDERNO"),
	DETAIL_NO("DETAILNO"),
	CONTROLLER("CONTROLLER"),
	BACKURL("BACKURL"),
	FRONTURL("FRONTURL"),
	REF("REF"),
	;
	
	private String key;

	private ControllerFieldType(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
	
	public static ControllerFieldType getTypeByCode(String code){
//		if(code == null){
//			return null;
//		}
		for (ControllerFieldType type : values()) {
			if(type.getKey().equalsIgnoreCase(code)){
				return type;
			}
		}
		throw new RuntimeException("Invalid controller-type.");
	}
}
