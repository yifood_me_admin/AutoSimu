package com.hundsun.epay.autosimu.internal;

import java.util.ArrayList;
import java.util.List;

import com.hundsun.epay.autosimu.config.ControllerResp;
import com.hundsun.epay.autosimu.internal.rules.RespControlRules;
import com.hundsun.epay.autosimu.utils.FileUtility;
import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 响应控制包装
 * @author hspcadmin
 *
 */
public class ControlRespPackaging {
	private ControllerResp resp;
	private List<RespControlRules> andRules;
	private String respContent;    //响应内容
	
	public ControlRespPackaging() {
		super();
		andRules = new ArrayList<RespControlRules>();
	}

	/**
	 * @return the resp
	 */
	public ControllerResp getResp() {
		return resp;
	}

	/**
	 * @param resp the resp to set
	 */
	public void setResp(ControllerResp resp) {
		this.resp = resp;
	}
	
	public void initialize(){
		String sourceRespRuleStr = resp.getRule();
		if(StringUtility.stringHasValue(sourceRespRuleStr)){
			String [] ruleStrs = sourceRespRuleStr.split(",");
			for (String ruleStr : ruleStrs) {
				andRules.add(new RespControlRules(ruleStr));
			}
		}
		if(StringUtility.stringHasValue(resp.getImportUrl())){
			try {
				respContent = FileUtility.readFileContent(resp.getImportUrl());
			} catch (Exception e) {
				throw new RuntimeException(String.format("Read template from url(%s) Exception.", resp.getImportUrl()));
			}
		}else{
			respContent = resp.getTemplate();
		}
	}

	/**
	 * @return the respContent
	 */
	public String getRespContent() {
		return respContent;
	}

	/**
	 * @return the andRules
	 */
	public List<RespControlRules> getAndRules() {
		return andRules;
	}
	
	
}
