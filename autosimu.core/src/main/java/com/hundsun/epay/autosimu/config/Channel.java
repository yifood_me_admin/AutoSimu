package com.hundsun.epay.autosimu.config;

import java.util.ArrayList;
import java.util.List;

import com.hundsun.epay.autosimu.enums.CommunicationMethod;
import com.hundsun.epay.autosimu.utils.StringUtility;

public class Channel {
	private String id;
	private CommunicationMethod commMethod;
	private List<Trade> trades;
	private String className;
	
	public Channel() {
		super();
		trades = new ArrayList<Trade>();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<Trade> getTrades() {
		return trades;
	}
	public void addTrade(Trade trade){
		trades.add(trade);
	}
	public Trade getTrade(String id){
		for (Trade trade : trades) {
			if(id.equals(trade.getId())){
				return trade;
			}
		}
		return null;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public void validate(List<String> errors, boolean hasComm){
		if(!StringUtility.stringHasValue(id)){
			errors.add("\"id\" is required in a context");
		}
		boolean hasCommMethod = hasComm || commMethod != null;
		for (Trade trade : trades) {
			trade.validate(errors, hasCommMethod);
		}
	}
	public CommunicationMethod getCommMethod() {
		return commMethod;
	}
	public void setCommMethod(CommunicationMethod commMethod) {
		this.commMethod = commMethod;
	}
}
