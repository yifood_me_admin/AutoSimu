package com.hundsun.epay.autosimu.internal;

import java.util.HashSet;
import java.util.Set;

import com.hundsun.epay.autosimu.utils.StringUtility;

/**
 * 家谱类
 * 
 * @author Clown
 * @Date 2018-05-04
 */
public class Genealogy {
	protected Set<Genealogy> children;
	protected String name;
	
	private Genealogy(String name) {
		this.name = name;
		this.children = new HashSet<Genealogy>(1);
	}
	
	public static Genealogy getRootGenealogy(){
		return new Genealogy(null);
	}

	/**
	 * 添加新成员到家谱中
	 * @param childName
	 * @return 新加入的成员节点
	 */
	public Genealogy addChild(String childName) {
		if(StringUtility.isBlank(childName)){
			throw new RuntimeException("Genealogy node need a node name");
		}
		Genealogy node = getGenealogy(childName);
		this.children.add(node);
		return node;
	}

	private Genealogy getGenealogy(String nodeName) {
		for (Genealogy genealogy : children) {
			if (genealogy.name.equals(nodeName)) {
				return genealogy;
			}
		}
		return new Genealogy(nodeName);
	}
	
	/**
	 * @return the children
	 */
	public Set<Genealogy> getChildren() {
		return children;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (name == null) {
			return 0;
		}
		return name.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Genealogy node = (Genealogy) obj;
		if (this.name == null) {
			return node.name == null;
		}

		return this.name.equals(node.name);
	}
}
