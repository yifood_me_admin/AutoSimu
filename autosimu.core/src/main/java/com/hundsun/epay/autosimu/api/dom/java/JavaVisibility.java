package com.hundsun.epay.autosimu.api.dom.java;

public enum JavaVisibility {
	PUBLIC("public "),
	PRIVATE("private "),
	PROTECTED("protected "),
	DEFAULT("")
	;
	private String desc;

	private JavaVisibility(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}
	
}
