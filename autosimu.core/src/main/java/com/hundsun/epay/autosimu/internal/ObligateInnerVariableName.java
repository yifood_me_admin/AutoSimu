package com.hundsun.epay.autosimu.internal;

/**
 * 预留内部变量名称
 * @author clown
 * @Date 2018-04-27
 */
public class ObligateInnerVariableName {
	/**预留订单号内部字段名*/
	public static final String OBLIGATE_ORDER_NO = "__orderNo";
	/**预留明细号内部字段名*/
	public static final String OBLIGATE_DETAIL_NO = "__detailNo";
	/**预留前台回调内部字段名*/
	public static final String OBLIGATE_FRONT_URL = "__frontUrl";
	/**预留后台回调内部字段名*/
	public static final String OBLIGATE_BACK_URL = "__backUrl";
	/**预留订单表对象*/
	public static final String OBLIGATE_ORDER_OBJECT = "__order";
	/**预留订单表控制域map/string 对象*/
	public static final String OBLIGATE_ORDER_FIELDS = "__fields";
	/**预留订单表控制域字段 解析对象*/
	public static final String OBLIGATE_FIELD_OBJECT = "__fieldObj";
	/**预留订单服务*/
	public static final String OBLIGATE_ORDER_SERVICE = "__orderService";
	/**预留通知对象*/
	public static final String OBLIGATE_NOTIFY_OBJECT = "__notify";
	/**预留通知信息*/
	public static final String OBLIGATE_NOTIFY_MSG = "__notifyMsg";
	/**预留待替换响应信息模板*/
	public static final String OBLIGATE_TEMPLATE_RESPMSG = "__respMsg";
	/**预留的请求数据对象*/
	public static final String OBLIGATE_REQ_OBJECT = "__reqObj";
	/**预留的请求数据变量*/
	public static final String OBLIGATE_REQ_DATA = "__reqData";
}
