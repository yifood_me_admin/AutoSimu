package com.hundsun.epay.autosimu.utils;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;

public class FileUtility {
	public static final String DEFAULT_CHARSET = "UTF-8";
	
	public static String readFileContent(String path) throws Exception{
		return readFileContent(path, DEFAULT_CHARSET);
	}
	
	public static String readFileContent(String path, String charset) throws Exception{
		FileInputStream fis = null;
		try {
			File file = new File(path);
			fis = new FileInputStream(file);
			int length = (int) file.length();
			byte[] cache = new byte[length];
			fis.read(cache);
			return new String(cache, charset);
		} finally{
			closeIO(fis);
		}
	}
	
	public static void closeIO(Closeable closeable){
		if(closeable != null){
			try {
				closeable.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
